variable "yc_cloud" {
  description = "Default cloud ID in yandex cloud"
  type        = string
  default     = ""
}

variable "yc_folder" {
  description = "Default folder ID in yandex cloud"
  type        = string
  default     = ""
}

variable "yc_token" {
  type        = string
  default     = ""
  description = "Default cloud token in yandex cloud"
  sensitive   = true
}

variable "user" {
  type        = string
  description = "andrei"
}

variable "zone" {
  description = "Use specific availability zone"
  type        = list(string)
  default     = ["ru-central1-a", "ru-central1-b", "ru-central1-d"]
}

variable "domain_name" {
  type        = string
  default     = "andreiplekhanov.ru"
  description = "Your site's name, for example 'andreiplekhanov.ru'"
}

variable "db_password" {
  type        = string
  default     = "123456qwerty"
  description = "Default password for PostgreSQL"
  sensitive   = true
}

variable "db_username" {
  type        = string
  default     = "db_username"
  description = "Default username for PostgreSQL"
  sensitive   = true
}

variable "db_name" {
  type        = string
  default     = "db_name"
  description = "Default name for PostgreSQL"
  sensitive   = true
}